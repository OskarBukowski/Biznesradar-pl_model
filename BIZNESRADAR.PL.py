import pandas as pd
from bs4 import BeautifulSoup as bs
import requests
import numpy as np
import matplotlib.pyplot as plt
import pandas_datareader.data as web
import warnings
import fpdf
from fpdf import FPDF
from PIL import Image
import scipy.stats as stats
import pylab

warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)


ticker = 'ALUMETAL'

stock = 'AML'

gpw_ticker = f'{stock}.PL'



rzis_web = f'https://www.biznesradar.pl/raporty-finansowe-rachunek-zyskow-i-strat/{ticker},Q'

bilans_web = f'https://www.biznesradar.pl/raporty-finansowe-bilans/{ticker},Q,0'

cash_web = f'https://www.biznesradar.pl/raporty-finansowe-przeplywy-pieniezne/{ticker},Q'

rynkowej_web = f'https://www.biznesradar.pl/wskazniki-wartosci-rynkowej/{ticker}'

rentownosci_web = f'https://www.biznesradar.pl/wskazniki-rentownosci/{ticker}'

przeplywow_web = f'https://www.biznesradar.pl/wskazniki-przeplywow-pienieznych/{ticker}'

zadluzenia_web = f'https://www.biznesradar.pl/wskazniki-zadluzenia/{ticker}'

plynnosci_web = f'https://www.biznesradar.pl/wskazniki-plynnosci/{ticker}'

aktywnosci_web = f'https://www.biznesradar.pl/wskazniki-aktywnosci/{ticker}'

ryzyko_web = f'https://www.biznesradar.pl/analiza-portfelowa/{ticker}'



def main(element, lista):
    
        
        
    req = requests.get(element)
        
        
    soup = bs(req.content, features="lxml")
        
        
    table = soup.find_all('table', attrs = {'class':'report-table'})
    df = pd.read_html(str(table))[0]
        
    df = df.transpose()
        
        
    df = df.rename(columns = df.iloc[0])
    df = df.fillna(method = 'backfill')
         
        
    df = df.drop(df.tail(1).index)
    df = df.drop(df.head(1).index)
            
    
            
            
    for i in range(len(list(lista))):
        
        df[lista[i]] = df[lista[i]].str.split('[rk~%]').str[0].str.replace(" ", '').astype(float)
        
            
    df.index = df.index.str.split().str[0]

    
    
    return df


rzis_list = ['Przychody ze sprzedaży',
          'Zysk ze sprzedaży',
          'Zysk operacyjny (EBIT)',
          'Zysk netto akcjonariuszy jednostki dominującej',
          'EBITDA']




rzis = main(rzis_web, rzis_list)



bilans_list = ['Aktywa trwałe',
            'Aktywa obrotowe',
            'Kapitał własny akcjonariuszy jednostki dominującej',
            'Zobowiązania długoterminowe',
            'Zobowiązania krótkoterminowe',
            'Wartość firmy']


bilans = main(bilans_web, bilans_list)



cash_list = ['Przepływy pieniężne z działalności operacyjnej',
          'Przepływy pieniężne z działalności inwestycyjnej',
          'CAPEX (niematerialne i rzeczowe)',
          'Przepływy pieniężne z działalności finansowej',
          'Dywidenda',
          'Skup akcji',
          'Free Cash Flow' ]



cash = main(cash_web, cash_list)


rynkowej_list = ['Kurs',
          'Cena / Wartość księgowa',
          'Cena / Przychody ze sprzedaży',
          'Cena / Zysk',
          'EV / Przychody ze sprzedaży',
          'EV / EBIT',
          'EV / EBITDA' ]


rynkowej = main(rynkowej_web, rynkowej_list)


rentownosci_list = ['ROE',
          'ROA',
          'Marża zysku netto',
          'Marża zysku ze sprzedaży',
          'ROIC' ]


rentownosci = main(rentownosci_web, rentownosci_list)


zadluzenia_list =  [ 'Zadłużenie ogólne',
          'Zadłużenie kapitału własnego',
            ] 

zadluzenia = main(zadluzenia_web, zadluzenia_list)



plynnosci_list = ['Płynność bieżąca']

plynnosci = main(plynnosci_web, plynnosci_list)



# WYKRESY#####################################################################


# Altman EM-Score   --------------------------

altman = pd.DataFrame()

altman['one'] = 6.65*((bilans['Aktywa obrotowe']-bilans['Zobowiązania krótkoterminowe'])/(bilans['Aktywa trwałe']+bilans['Aktywa obrotowe']))
altman['two'] = 3.26*((rzis['Zysk netto akcjonariuszy jednostki dominującej']-cash['Dywidenda'])/(bilans['Aktywa trwałe']+bilans['Aktywa obrotowe']))
altman['three'] = 6.72*(rzis['Zysk operacyjny (EBIT)']/(bilans['Aktywa trwałe']+bilans['Aktywa obrotowe']))
altman['four'] = 1.05*(bilans['Kapitał własny akcjonariuszy jednostki dominującej']/(bilans['Zobowiązania krótkoterminowe']+ bilans['Zobowiązania długoterminowe']))

altman = altman.fillna(method = 'backfill')

altman['em_score'] = altman['one'] + altman['two'] + altman['three'] + altman['four'] + 3.25

xticks_alt = altman.index[::4]


plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))
plt.title('ALTMAN EM-SCORE',fontsize = 20)
plt.plot(altman['em_score'], color='r', linestyle = '-', linewidth= 3)
plt.grid(axis = 'x', alpha = 0.5)
plt.xticks(xticks_alt, fontsize = 13)
plt.yticks(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('altman_2.JPG')
# plt.show()

# Zadłużenie ogółem  |   Zadłużenie kapitału własnego   ------------------

xticks_zad = zadluzenia.index[::4]
yticks = np.arange(0.0,2.5,0.25)

plt.style.use('dark_background')
plt.get_cmap('twilight')


plt.figure(figsize = (22,8))
plt.title('WSKAŹNIKI ZADŁUŻENIA',fontsize = 20)
plt.plot(zadluzenia['Zadłużenie kapitału własnego'], color='r', linestyle = '-', linewidth= 3, label = 'Zadłużenie kapitału własnego' )
plt.plot(zadluzenia['Zadłużenie ogólne'], color='C2', linestyle = '-', linewidth= 3, label = 'Zadłużenie ogólne')
plt.grid(axis = 'y', alpha=0.5)
plt.xticks(xticks_zad, fontsize = 13)
plt.yticks(yticks, fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('zadluzenia.JPG')
# plt.show()

# Wskaźnik bieżącej płynnosci   -------------------------------------------


xticks_pln = plynnosci.index[::4]

plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))
plt.title('WSKAŹNIK PŁYNNOŚCI BIEŻĄCEJ',fontsize = 20)
plt.plot(plynnosci['Płynność bieżąca'], color='c', linestyle = '-', linewidth= 3)
plt.grid(axis = 'y', alpha=0.5)
plt.xticks(xticks_pln, fontsize=13)
plt.yticks(fontsize = 14)


plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('plynnosci.JPG')
# plt.show()


# EV/EBITDA,REVENUE  |  Cena akcji -------------------------------------------

xticks_ev = rynkowej.index[::4]



plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))
plt.title('WSKAŹNIKI WYCENY PRZEDSIĘBIORSTWA',fontsize = 20)
plt.plot(rynkowej['EV / EBITDA'], color='c', linestyle = '-', linewidth= 3, label = 'EV/EBITDA')
plt.plot(rynkowej['EV / Przychody ze sprzedaży'], color='r', linestyle = '-', linewidth= 3, label = 'EV/REVENUE')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(xticks_ev, fontsize=12)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 12)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)


plt.savefig('rynkowej.JPG')
# plt.show()


# ROE, ROA, ROS, ROIC, Marża zysku ze sprzedaży   -------------------------



xticks_rent = rentownosci.index[::4]

# 1


plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))
plt.title('WSKAŹNIKI RENTOWNOŚCI',fontsize = 20)
plt.plot(rentownosci['ROE'], color='c', linestyle = '-', linewidth= 3, label = 'ROE')
plt.plot(rentownosci['ROA'], color='r', linestyle = '-', linewidth= 3, label = 'ROA')
plt.plot(rentownosci['ROIC'], color='m', linestyle = '-', linewidth= 3, label = 'ROIC')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(xticks_rent, fontsize=12)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('rentownosci.JPG')

# # 2


plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))
plt.title('MARŻA ZYSKU ZA SPRZEDAŻY',fontsize = 20)
plt.plot(rentownosci['Marża zysku ze sprzedaży'], color='c', linestyle = '-', linewidth= 3, label = 'Marża zysku ze sprzedaży')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(xticks_rent, fontsize=12)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('marza.JPG')


# P/E,BV,REVENUE    |  Cena akcji ------------------------------------------

xticks_ryn = rynkowej.index[::4]

plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))

plt.title('WSKAŹNIKI WARTOŚCI RYNKOWEJ',fontsize = 20)
plt.plot(rynkowej['Cena / Zysk'], color='r', linestyle = '-', linewidth= 3, label = 'P/E')
plt.plot(rynkowej['Cena / Wartość księgowa'], color='C2', linestyle = '-', linewidth= 3, label = 'P/BV')
plt.plot(rynkowej['Cena / Przychody ze sprzedaży'], color='c', linestyle = '-', linewidth= 3, label = 'P/REVENUE')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(xticks_ryn, fontsize=12)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('akcji_rynkowej.JPG')

# Zysk ze sprzedaży  |  EBIT -------------------------------------------

xticks_rzis = rzis.index[::4]


plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))

plt.title('WYNIKI SPRZEDAŻY',fontsize = 20)
plt.plot(rzis['Zysk ze sprzedaży'], color='r', linestyle = '-', linewidth= 3, label = 'Zysk ze sprzedaży')
plt.plot(rzis['Zysk operacyjny (EBIT)'], color='C2', linestyle = '-', linewidth= 3, label = 'EBIT')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(xticks_rzis, fontsize=12)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)


plt.savefig('zyskownosci.JPG')


# Cena akcji  |  Przychody   ------------------------------------------

porownanie = pd.DataFrame()

porownanie['Kurs'] = rynkowej['Kurs']
porownanie['Przychody ze sprzedaży'] = rzis['Przychody ze sprzedaży']

porownanie = porownanie.loc[porownanie['Kurs'] > 0.0] # usuwam okresy początkowe zaburzające dane w czasie gdy akcje nie były w obiegu


xticks_por = porownanie.index[::4]




fig, ax1 = plt.subplots()

fig.set_figheight(8)
fig.set_figwidth(18)

plt.title('REAKCJA KURSU AKCJI NA ZMIANY W PRZYCHODACH ZE SPRZEDAŻY', fontsize = 20)

ax1.plot(porownanie['Kurs'], color = 'r')
ax1.set_xticks(xticks_por)
ax1.set_xticklabels(xticks_por)
ax1.spines['top'].set_visible(False)
ax1.tick_params(axis = 'x', labelsize = 14)
ax1.tick_params(axis = 'y', labelsize = 14, labelcolor = 'r')
ax1.grid(alpha = 0.5)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis


ax2.plot(porownanie['Przychody ze sprzedaży'], color = 'C2')
ax2.set_xticks(xticks_por)
ax2.set_xticklabels(xticks_por, color = 'C2')
ax2.spines['top'].set_visible(False)
ax2.tick_params(axis = 'x', labelsize = 14)
ax2.tick_params(axis = 'y', labelsize = 14, labelcolor = 'C2')
ax2.grid(alpha = 0.0)

fig.tight_layout()


plt.savefig('porownanie.JPG')

### STATYSTYKA ###############################################################


asset = web.DataReader(gpw_ticker, 'stooq')

# print(asset)

asset = asset.drop(columns = ['Open', 'High', 'Low'])
asset['Delta'] = asset['Close'].pct_change()


asset['Date'] = pd.to_datetime(asset.index)
asset = asset.set_index('Date', drop=True)



plt.figure(figsize = (18,8))


up_fig = plt.subplot2grid((5,4), (0, 0), rowspan=4, colspan=5)
up_fig.plot(asset['Close'], color = 'r', linewidth=3)
plt.grid(alpha = 0.5)
plt.title(label = 'KURS AKCJI', fontsize = 20)
plt.setp(up_fig.get_xticklabels(), visible=False)

bottom_fig = plt.subplot2grid((5,4), (4,0), rowspan=2, colspan=4, sharex= up_fig)
bottom_fig.bar(asset.index , asset['Volume'])

plt.savefig('akcja_wolumen.JPG')

# Rozkład stóp zwrotu ----------------------------------------------

fig = plt.figure(figsize=(14, 6))
ax1 = fig.add_subplot(1, 1, 1)
asset['Delta'].hist(bins=50, ax=ax1)
ax1.set_xlabel('Return', fontsize=14)
ax1.set_ylabel('Frequency', fontsize=14)
ax1.set_title('ROZKŁAD STÓP ZWROTU', fontsize=20)
ax1.tick_params(axis = 'x', labelsize = 14)
ax1.tick_params(axis = 'y', labelsize = 14)
plt.grid(alpha = 0.5)

plt.savefig('rozklad.JPG')
plt.show()

#  risk measures

log_returns = np.log(asset.Close/asset.Close.shift(1)).fillna(method = 'backfill')

annualized_std = round(log_returns.std() * np.sqrt(252),4)

mean = round(log_returns.mean(), 4)

skewness = round(log_returns.skew(), 4)

kurtosis = round(log_returns.kurtosis(), 4)

max_value = round(log_returns.max(), 4)

min_value = round(log_returns.min(), 4)


# ---------------------------------------

days = 252

trailing_volatility = log_returns.rolling(window = days).std() * np.sqrt(days)



plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))

plt.title('VOLATILITY',fontsize = 20)
plt.plot(trailing_volatility, color='r', linestyle = '-', linewidth= 3, label = 'Rolliing volatility')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(fontsize=14)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('rolling_volatility.JPG')
plt.show()



# ----------------------------------------

rfr = 0.01 / 252  # daily risk free rate
mean_return = log_returns.rolling(window = days).mean()

sharpe_ratio = (mean_return - rfr) * 252 / trailing_volatility



plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))

plt.title('SHARPE RATIO',fontsize = 20)
plt.plot(sharpe_ratio, color='r', linestyle = '-', linewidth= 3, label = 'Rolliing Sharpe Ratio')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(fontsize=14)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('rolling_sharpe.JPG')
plt.show()

# ----------------------------------------

sortino_vol = log_returns[log_returns<0].rolling(window = days, center = True, min_periods = 5).std()* np.sqrt(days)


sortino_ratio = (mean_return - rfr) * 252 / sortino_vol


plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))

plt.title('SORTINO RATIO',fontsize = 20)
plt.plot(sortino_ratio, color='r', linestyle = '-', linewidth= 3, label = 'Rolliing Sortino Ratio')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(fontsize=14)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('rolling_sortino.JPG')
plt.show()

# --------------------------------------------------------------------

bench = web.DataReader('WIG20.PL', 'stooq')

bench = bench.drop(columns = ['Open', 'High', 'Low'])


m2_ratio = pd.DataFrame()

log_bench_returns = np.log(bench.Close/bench.Close.shift(1)).fillna(method = 'backfill')

benchmark_vol = log_bench_returns.rolling(window = days).std() * np.sqrt(days)

m2_ratio = (sharpe_ratio*benchmark_vol/ days + rfr)* days



plt.style.use('dark_background')
plt.get_cmap('twilight')

plt.figure(figsize = (18,8))

plt.title('MODIGLIANI RATIO',fontsize = 20)
plt.plot(m2_ratio, color='r', linestyle = '-', linewidth= 3, label = 'Rolliing M2 Ratio')
plt.grid(axis = 'y', alpha=0.5)

plt.xticks(fontsize=14)
plt.yticks(fontsize = 14)
plt.legend(fontsize = 14)

plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)

plt.savefig('rolling_modigliani.JPG')

def max_drawdown(returns):
    cumulative_returns = (1+returns).cumprod()
    peak = cumulative_returns.expanding(min_periods = 1).max()
    drawdown = (cumulative_returns / peak) - 1
    
    return drawdown.min()


returns = asset.Close.pct_change().dropna()

drawdown = round((max_drawdown(returns)*100),4)




calmar = round((np.exp(log_returns.mean()* 252) / abs(max_drawdown(returns))),4)


def historicalVAR(log_returns, alpha = 1) :
    if isinstance(log_returns, pd.Series) :
        return np.percentile(returns, alpha)

    elif isinstance(log_returns, pd.DataFrame) :
        return returns.aggregate(historicalVAR, alpha = 1)
    
    else :
        raise TypeError('Expected returns to be dataframe or series')
        

        
        
def historicalCVAR(returns, alpha = 1)  :

    if isinstance(returns, pd.Series) :
        belowVAR = returns <= historicalVAR(returns, alpha = 1)
        return returns[belowVAR].mean()

    elif isinstance(returns, pd.DataFrame) :
        return returns.aggregate(historicalCVAR, alpha = 1)

    
    else :
        raise TypeError('Expected returns to be dataframe or series')        
        

hist_var = round((historicalVAR(log_returns, alpha = 1)),4)
hist_cvar = round((historicalCVAR(log_returns, alpha = 1)),4)


  

####################################################
####################################
##########################
#####################

# PDF


max_height = 297
max_width = 210

name_1 = 'akcja_wolumen.JPG'
name_2 = 'akcji_rynkowej.JPG'
name_3 = 'porownanie.JPG'
name_4 = 'zyskownosci.JPG'
name_5 = 'rynkowej.JPG'
name_6 = 'rentownosci.JPG'
name_7 = 'marza.JPG'
name_8 = 'altman_2.JPG'
name_9 = 'zadluzenia.JPG'
name_10 = 'plynnosci.JPG'
name_11 = 'rozklad.JPG'
name_12 = 'rolling_volatility.JPG'
name_13 = 'rolling_sharpe.JPG'
name_14 = 'rolling_sortino.JPG'
name_15 = 'rolling_modigliani.JPG'
name_16 = 'normality_plot.JPG'

img = Image.new('RGB', (58,58), "#3a3a3a" )
img.save('grey_colored.png')

img = Image.new('RGB', (36,36), "#242424" )
img.save('black_colored.png')




def create_title(pdf):
    pdf.set_font('helvetica', 'B', 22)
    pdf.set_draw_color(36,36,36)
    pdf.set_fill_color(36,36,36)
    pdf.set_text_color(255,250,250)
    pdf.set_y(0)
    pdf.set_x(0)
    pdf.cell(max_width, 20, f'Report for company {ticker}', fill = True, align='C')
    pdf.image('black_colored.png', 0, 287 , max_width, 10)

     

def create_report(filename = 'RAPORT.pdf'):
    pdf = FPDF('P', 'mm', 'A4')    
    pdf.add_page() 
    
    pdf.image('grey_colored.png', x = 0, y = 0, w = 210, h = 297, type = '', link = '')
    
    create_title(pdf)
    
    
           
    pdf.image(name_1, 10, 40 , max_width-20, 100)
    
    pdf.image(name_2, 10, 160 , max_width-20, 100)

                
    pdf.add_page()
    pdf.image('grey_colored.png', x = 0, y = 0, w = 210, h = 297, type = '', link = '')
       
    pdf.image('black_colored.png', 0, 0 , max_width, 10)           
    pdf.image(name_3, 10, 20 , max_width-20, 100)
    pdf.image(name_4, 10, 150 , max_width-20, 100)
    pdf.image('black_colored.png', 0, 287 , max_width, 10) 
              
    pdf.add_page()
    pdf.image('grey_colored.png', x = 0, y = 0, w = 210, h = 297, type = '', link = '')
      
    pdf.image('black_colored.png', 0, 0 , max_width, 10)           
    pdf.image(name_5, 10, 20 , max_width-20, 75)
    pdf.image(name_6, 10, 110 , max_width-20, 75)
    pdf.image(name_7, 10, 200 , max_width-20, 75)
    pdf.image('black_colored.png', 0, 287 , max_width, 10)
               
    pdf.add_page()
    pdf.image('grey_colored.png', x = 0, y = 0, w = 210, h = 297, type = '', link = '')
    
    pdf.image('black_colored.png', 0, 0 , max_width, 10)             
    pdf.image(name_8, 10, 20 , max_width-20, 75)
    pdf.image(name_9, 10, 110 , max_width-20, 75)
    pdf.image(name_10, 10, 200 , max_width-20, 75)
    pdf.image('black_colored.png', 0, 287 , max_width, 10)
                
    pdf.add_page()
    pdf.image('grey_colored.png', x = 0, y = 0, w = 210, h = 297, type = '', link = '')
    
    
    
    pdf.set_y(0)
    pdf.set_x(0)
    pdf.set_font('helvetica', 'B', 20)
    pdf.set_fill_color(36,36,36)
    pdf.cell(max_width, 15,'Statistics', ln = True, align = 'C', fill = True)   
       
    pdf.image(name_11, 65, 20 , max_width-70, 90)
    
    
    pdf.set_font('helvetica', 'I', 12)
    pdf.set_text_color(255,250,250)
    
    

    pdf.set_y(25)
    pdf.cell(50,10,f'Mean : {mean}', ln = True, align = 'L')
    

    pdf.set_y(40)
    pdf.cell(50,10,f'Standard deviation : {annualized_std}', ln = True, align = 'L')
    
    
    

    pdf.set_y(55)
    pdf.cell(50,10,f'Skewness : {skewness}', ln = True, align = 'L')
    
    

    pdf.set_y(70)
    pdf.cell(50,10,f'Kurtosis : {kurtosis}', ln = True, align = 'L')
    

    pdf.set_y(85)
    pdf.cell(50,10,f'Minimum : {min_value}', ln = True, align = 'L')
    
        

    pdf.set_y(100)
    pdf.cell(50,10,f'Maximum : {max_value}', ln = True, align = 'L')
    
    
    pdf.image(name_12, 5, 115 , max_width-10, 90)
    
    pdf.set_font('helvetica', 'B', 16)
    pdf.set_y(210)
    
    pdf.cell(80,8,'With the given confidence level 0.01 :', ln = True, align = 'L')
    
    pdf.set_font('helvetica', 'I', 14)
    pdf.set_y(230)
    pdf.cell(80,8,f'Historical Value at Risk : {hist_var}', ln = True, align = 'L')
    
    pdf.set_y(245)
    pdf.cell(80,8,f'Expected Historical Shortfall : {hist_cvar}', ln = True, align = 'L')
        
    pdf.set_y(230)
    pdf.set_x(80)
    pdf.cell(160,8,f'Max Drawdown : {drawdown}', ln = True, align = 'C')   
    
    pdf.set_y(245)
    pdf.set_x(80)
    pdf.cell(160,8,f'Calmar Ratio : {calmar}', ln = True, align = 'C')   
    

    
    pdf.image('black_colored.png', 0, 287 , max_width, 10)
    
    pdf.add_page()
    pdf.image('grey_colored.png', x = 0, y = 0, w = 210, h = 297, type = '', link = '')
    
    pdf.image('black_colored.png', 0, 287 , max_width, 15)
    
    pdf.set_y(0)
    pdf.set_x(0)
    pdf.set_font('helvetica', 'B', 20)
    pdf.set_text_color(255,250,250)
    pdf.set_fill_color(36,36,36)
    pdf.cell(max_width, 15,'Risk ratios', ln = True, align = 'C', fill = True)
    
    pdf.image(name_13, 10, 20 , max_width-20, 75)
    
    pdf.image(name_14, 10, 110 , max_width-20, 75)
    
    pdf.image(name_15, 10, 200 , max_width-20, 75)
       
    
    pdf.image('black_colored.png', 0, 287 , max_width, 10)
    
    pdf.output(filename)
 

if __name__ == '__main__':
     create_report() 