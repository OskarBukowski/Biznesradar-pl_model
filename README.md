# Biznesradar.pl_model


Attached file is a simple script to create the comprehensive report about the financial/market/quantitative situation of given stock listed on Warsaw Stock Exchange.

Main element is webscrapping from website biznesradar.pl, and some pandas, matplotlib etc.
There you can find a bit of match approach like VaR and Expected Shortfall.


There are only two strings needed to run this script.

You have to write :

ticker = "....."  --> the name of company you want to appear on the title of report


stock = "...." --> the 3-letter shortcut unique for each stock from WSE


![image](https://user-images.githubusercontent.com/89335034/143939374-4e276a64-00f4-497b-b505-2688b6e8f41d.png)
